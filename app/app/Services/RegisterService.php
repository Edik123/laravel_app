<?php

namespace App\Services;

use App\Models\User;

class RegisterService
{
    public function verify($id): void
    {
        /** @var User $user */
        $user = User::findOrFail($id);
        $user->verify();
    }
}
